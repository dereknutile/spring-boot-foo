package com.dereknutile.springbootfoo.service;

import com.dereknutile.springbootfoo.dao.PersonDao;
import com.dereknutile.springbootfoo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    private final PersonDao personDao;

//    Using Autowired with the Qualifier allows a variable named connection - fakeDao, which could easily now be
//    replaced with a DAO named something else like mongo or mssql
    @Autowired
    public PersonService(Qualifier("fakeDao") PersonDao personDao) {
        this.personDao = personDao;
    }

    public int addPerson(Person person){
        return personDao.insertPerson(person);
    }
}
