package com.dereknutile.springbootfoo.dao;

import com.dereknutile.springbootfoo.model.Person;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository("fakeDao")
abstract public class FakePersonDataAccessService implements PersonDao {

    private static final List<Person> DB = new ArrayList();

    @Override
    public int insertPerson(UUID id, Person person) {
        return 0;
    }
}
