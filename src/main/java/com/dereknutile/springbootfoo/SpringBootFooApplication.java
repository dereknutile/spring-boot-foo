package com.dereknutile.springbootfoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootFooApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootFooApplication.class, args);
	}

}
